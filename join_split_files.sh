#!/bin/bash

cat system/system/priv-app/ANXCamera/ANXCamera.apk.* 2>/dev/null >> system/system/priv-app/ANXCamera/ANXCamera.apk
rm -f system/system/priv-app/ANXCamera/ANXCamera.apk.* 2>/dev/null
cat system/system/etc/ANXCamera/mimoji/model.zip.* 2>/dev/null >> system/system/etc/ANXCamera/mimoji/model.zip
rm -f system/system/etc/ANXCamera/mimoji/model.zip.* 2>/dev/null
cat system/system/apex/com.android.art.release.apex.* 2>/dev/null >> system/system/apex/com.android.art.release.apex
rm -f system/system/apex/com.android.art.release.apex.* 2>/dev/null
cat system/system/apex/com.android.vndk.current.apex.* 2>/dev/null >> system/system/apex/com.android.vndk.current.apex
rm -f system/system/apex/com.android.vndk.current.apex.* 2>/dev/null
cat system/system/product/app/webview/webview.apk.* 2>/dev/null >> system/system/product/app/webview/webview.apk
rm -f system/system/product/app/webview/webview.apk.* 2>/dev/null
cat system/system/system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null >> system/system/system_ext/priv-app/Settings/Settings.apk
rm -f system/system/system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null
cat system/system/system_ext/apex/com.android.vndk.v29.apex.* 2>/dev/null >> system/system/system_ext/apex/com.android.vndk.v29.apex
rm -f system/system/system_ext/apex/com.android.vndk.v29.apex.* 2>/dev/null
